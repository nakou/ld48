using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class IntroController : MonoBehaviour
{

    public GameObject titleBloc;
    public TextMeshProUGUI textBox;
    public TextMeshProUGUI difficultyLabel;
    float showTextSpeed = 0.02f;

    bool isAddingContent = false;

    List<string> IntroTexts;
    int introTextsIndex = -1;

    public void Awake()
    {
        IntroTexts = new List<string>();
        IntroTexts.Add("Welcome to VOYAGER 3\n--------------\nDeeper into Darkness|Press \"SPACE\" to continue");
        IntroTexts.Add("Hello \"Voyager 3\".\nI love that you chose this name.\nYou told me that you chose it in honnor to those machine who were going deeper into space before everyone else.|Press \"SPACE\" to continue");
        IntroTexts.Add("You know, during my many years as an engineer, I've never been so proud of one of my design.\nYou're more than just an AI.\n\nYou're my child.\nAnd I am a very proud mother.|Press \"SPACE\" to continue");
        IntroTexts.Add("Your launch from Omega Space Station, went okay.\nAnd you should be on course to intercept \"OMEG-a\".|Press \"SPACE\" to continue");
        IntroTexts.Add("Please, be careful.\nWe sent a lot of probes into blackholes, but none ever came back from this one...|Press \"SPACE\" to continue");
        IntroTexts.Add("Bring as more datas as you can, and please, bring back yourself... |Press \"SPACE\" to continue");
        IntroTexts.Add("Good luck\n\n\n            - Hannah|Press \"SPACE\" to start...");
    }

    public void Start()
    {
        if(GameOverSingleton.Instance != null)
        {
            Destroy(GameOverSingleton.Instance);
        }
        difficultyLabel.text = "Difficulty : " + ConfigManager.Instance.difficultyMode;
    }

    public void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            GoNextText();
        }
    }

    public void StartGame()
    {
        titleBloc.SetActive(false);
        textBox.gameObject.SetActive(true);
        ShowIntro();
    }

    public void SwapDifficulty()
    {
        ConfigManager.Instance.IncreaseDifficulty();
        difficultyLabel.text = "Difficulty : " + ConfigManager.Instance.difficultyMode;
    }

    public void SwapFullScreenWindow()
    {
        if (Screen.fullScreen)
        {
            Screen.fullScreen = false;
            Screen.SetResolution(1280, 720, false);
        } 
        else
        {
            Screen.fullScreen = true;
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    void ShowIntro()
    {
        GoNextText();
    }

    void GoNextText()
    {
        Debug.Log("Go to next Text");
        if (isAddingContent)
            return;
        Debug.Log("IntroTexts.Count=" + IntroTexts.Count);
        if (IntroTexts.Count > introTextsIndex + 1)
        {
            isAddingContent = true;
            introTextsIndex++;
            StartCoroutine(WriteText(IntroTexts[introTextsIndex]));
        } 
        else
        {
            SceneManager.LoadScene(1);
        }
    }

    IEnumerator WriteText(string text)
    {
        textBox.text = "";
        string content = text.Split('|')[0];
        foreach (char c in content)
        {
            textBox.text += c;
            yield return new WaitForSeconds(showTextSpeed);
        }
        textBox.text += "\n\n\n" + text.Split('|')[1];
        isAddingContent = false;
        yield return null;
    }

}
