﻿using UnityEngine;
using System.Collections;

public class ConfigManager : MonoBehaviour
{

    #region Singleton
    private static ConfigManager instance;
    public static ConfigManager Instance { get { return instance; } }

    private void Awake()
    {
        Debug.Log("ConfigManager is Awaking!");
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }
    #endregion

    public float difficultyDamagesMultiplicator = 1f;
    public float difficultyHealingMultiplicator = 1f;

    public string difficultyMode = "Normal";

    public void IncreaseDifficulty()
    {
        if (difficultyMode.Equals("Nightmare"))
        {
            difficultyDamagesMultiplicator = .25f;
            difficultyHealingMultiplicator = 3f;
            difficultyMode = "Very Easy";
            return;
        }
        if (difficultyMode.Equals("Very Easy"))
        {
            difficultyDamagesMultiplicator = .50f;
            difficultyHealingMultiplicator = 2f;
            difficultyMode = "Easy";
            return;
        }
        if (difficultyMode.Equals("Easy"))
        {
            difficultyDamagesMultiplicator = 1f;
            difficultyHealingMultiplicator = 1f;
            difficultyMode = "Normal";
            return;
        }
        if (difficultyMode.Equals("Normal"))
        {
            difficultyDamagesMultiplicator = 1.5f;
            difficultyHealingMultiplicator = 1.1f;
            difficultyMode = "Challenging";
            return;
        }
        if (difficultyMode.Equals("Challenging"))
        {
            difficultyDamagesMultiplicator = 2f;
            difficultyHealingMultiplicator = 1.25f;
            difficultyMode = "Nightmare";
            return;
        }
    }

}
