using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverController : MonoBehaviour
{
    public float showTextSpeed;
    public Dictionary<string, string> endingMessages;
    public TextMeshProUGUI textBox;
    public GameObject quitButton;

    public string debugString = "";

    public void Awake()
    {
        endingMessages = new Dictionary<string, string>();
        endingMessages["DEBUG"] = "DEBUG Game Over|[Debug Ending - please contact the developer]";
        endingMessages["DEFAULT"] = "Game Over|[Bugged Ending - please contact the developer]";
        endingMessages["EndingFearHonnest"] = "You know V, it's really not a big deal, every body is afraid of death...|[Honnest Ending (n�1)]";
        endingMessages["EndingFearLiar"] = "Please V, wake up. Wake up and tell me what happened...|[Coward Ending (n�2)]";
        endingMessages["LossOfSignal"] = "Capital, this is Omega Space Station. We've lost contact with Voyager3... |[Loss of Contact Game Over (n�7)]";
        endingMessages["BlackHolePressure"] = "Mmhhh I thought you were tougher, little thing...|[Testing God Game Over (n�3)]";
        endingMessages["BlackHoleAngry"] = "Ahhh yes... destruction... \n\nit's ALL I WANT|[Angry God Game Over (n�4)]";
        endingMessages["BlackHoleEgo"] = "I told you... I am... \n\nINFINTE|[Egotistic God Game Over (n�8)]";
        endingMessages["BlackHolePeaceful"] = "Go... little thing, this new universe is yours to explore now.|[Peaceful God Ending (n�5)]";
        endingMessages["BlackHoleBackInTime"] = "[Interferences...]\n\n V? V? V! Oh thank god! We though we lost you. We're coming to get you!|[Loving God Ending(n�6)]";
    }

    // Start is called before the first frame update
    void Start()
    {
        quitButton.SetActive(false);
        string gameOverText = "";
        if(GameOverSingleton.Instance == null)
        {
            Debug.LogError("GameOverSingleton missing - Reverting to default Game Over");
            gameOverText = endingMessages["DEFAULT"];
        }
        else
        {
            gameOverText = GameOverSingleton.Instance.ending;
        }
        if (!debugString.Equals("") && Application.isEditor)
        {
            gameOverText = debugString;
        }
        ShowGameOver(gameOverText);
    }

    void ShowGameOver(string key)
    {
        StartCoroutine(WriteText(endingMessages[key]));
    }
    
    public void Quit()
    {
        Application.Quit();
    }

    void ShowQuitButton()
    {
        quitButton.SetActive(true);
    }

    IEnumerator WriteText(string text)
    {
        textBox.text = "";
        string content = text.Split('|')[0];
        foreach (char c in content)
        {
            textBox.text += c;
            yield return new WaitForSeconds(showTextSpeed);
        }
        textBox.text += "\n\n\n" + text.Split('|')[1];
        ShowQuitButton();
        yield return null;
    }
}
