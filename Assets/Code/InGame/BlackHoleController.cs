using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHoleController : MonoBehaviour
{
    public GameObject eventHorizon;
    public GameObject singularity;

    private void Update()
    {
        
    }

    public void FixedUpdate()
    {
        if (singularity.activeSelf)
        {
            singularity.transform.Rotate((Vector3.forward + Vector3.right) * 10f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<VoyagerController>() != null)
        {
            StoryManager.Instance.hasCollidedWithBlackHole = true;
            GameManager.Instance.ChangeSkyBoxAndMusic();
            eventHorizon.SetActive(false);
            StoryManager.Instance.StopVoyager(2f);
        }
    }

    public void ActivateSingularity()
    {
        singularity.SetActive(true);
    }
}
