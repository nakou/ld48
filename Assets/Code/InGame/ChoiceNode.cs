﻿public class ChoiceNode : AbstractNode
{
    private string answer1;
    private string answer2;
    private int answer1ComNodeId;
    private int answer2ComNodeId;

    public ChoiceNode(string answer1, int answer1ComNodeId, string answer2 = "", int answer2ComNodeId = -1, int nextNode = -1) : base(nextNode)
    {
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer1ComNodeId = answer1ComNodeId;
        this.answer2ComNodeId = answer2ComNodeId;
    }

    public override void ExecuteNode()
    {
        if (!string.IsNullOrEmpty(answer1))
            UIManager.Instance.AddAnswerOne(answer1, answer1ComNodeId);
        if (!string.IsNullOrEmpty(answer2))
            UIManager.Instance.AddAnswerTwo(answer2, answer2ComNodeId);
    }
}