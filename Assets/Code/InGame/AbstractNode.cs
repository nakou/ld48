﻿using UnityEngine;

public abstract class AbstractNode
{
    protected int nextNodeId = -1;

    public AbstractNode(int nextNodeId =  -1)
    {
        this.nextNodeId = nextNodeId;
    }

    public virtual void SetNextNode(int nextNodeId)
    {
        this.nextNodeId = nextNodeId;
    }

    public virtual void GoNextNode()
    {
        StoryManager.Instance.ExecuteNextNode(nextNodeId);
    }

    public abstract void ExecuteNode();
}