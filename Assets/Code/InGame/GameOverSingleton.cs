﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameOverSingleton : MonoBehaviour
{

    #region Singleton
    private static GameOverSingleton instance;
    public static GameOverSingleton Instance { get { return instance; } }

    private void Awake()
    {
        Debug.Log("GameOverSingleton is Awaking!");
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }
    #endregion

    public string ending;
    public bool isGameOver = false;

    public void GameOver(string ending)
    {
        if (isGameOver)
            return;
        this.ending = ending;
        if (UIManager.Instance != null)
            UIManager.Instance.StartGameOverEffect();
        Invoke("LoadGameOverScene", 3f);
        isGameOver = true;
    }

    public void LoadGameOverScene()
    {
        SceneManager.LoadScene(2);
    }

}
