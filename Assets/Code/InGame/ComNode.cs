﻿using System;

public class ComNode : AbstractNode
{
    public ComsOrigin comsOrigin;
    public string content;
    public float waitTime;
    public Action eventMethod;

    public ComNode(string content, ComsOrigin comsOrigin, float waitTime, int nextNodeId = -1, Action eventMethod = null) : base(nextNodeId)
    {
        this.content = content;
        this.comsOrigin = comsOrigin;
        if (GameManager.Instance.debugSpeedText)
            waitTime = .5f;
        this.waitTime = waitTime;
        this.eventMethod = eventMethod;
    }

    public override void ExecuteNode()
    {
        UIManager.Instance.AddComText(this);
    }

    public override void GoNextNode()
    {
        if (eventMethod != null)
            eventMethod.Invoke();
        base.GoNextNode();
    }
}