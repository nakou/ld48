﻿public enum ComsOrigin
{
    Voyager,
    Humans,
    System,
    Unknown,
    BlackHole
}
