﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class StoryManager : MonoBehaviour
{

    #region Singleton
    private static StoryManager instance;
    public static StoryManager Instance { get { return instance; } }

    private void Awake()
    {
        Debug.Log("StoryManager is Awaking!");
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        InitManager();
    }
    #endregion

    private AbstractNode currentNode;
    public Dictionary<int, AbstractNode> nodeList = new Dictionary<int, AbstractNode>();
    public int lastNodeId = -1;

    private string gamePhase;
    public string GamePhase 
    {
        get => gamePhase;
        set { gamePhase = value; Debug.Log("Loading new story line : " + gamePhase); }
    }
    public VoyagerController voyager;

    [Header("StoryElements")]
    public bool canAccelerate = false;
    public bool mustResistToPressure = false;
    public bool voyagerIsMoving = false;
    public bool hasCollidedWithBlackHole = false;
    public bool mustShowCourage = false;

    private bool waitForDistanceReached = false;
    private float distanceToReach = 0;
    private Action distanceEvent;

    void InitManager()
    {
        InitStoryIntro();
    }

    // I have to set those backward because of references, and this is AWEFUL. Didn't thought this through huh?
    // NIGHTMARE NIGHTMARE NIGHTMARE NIGHTMARE NIGHTMARE NIGHTMARE NIGHTMARE NIGHTMARE NIGHTMARE NIGHTMARE NIGHTMARE NIGHTMARE
    void InitStoryIntro()
    {
        GamePhase = "Intro";
        voyagerIsMoving = true;
        nodeList.Clear();
        // DEBUG EVENTS
        //nodeList[0] = new ComNode("Omega space station, this is voyager, do you copy? Over.", ComsOrigin.Voyager, 0f, 1, () => GravitationalEvent(true));
        //nodeList[1] = new ComNode("Voyager, this is Omega Space Station, we read you loud and clear, over.", ComsOrigin.Humans, 10f, 2, () => GravitationalEvent(false));

        nodeList[0] = new ComNode("Omega space station, this is voyager, do you copy? Over.", ComsOrigin.Voyager, 5f, 1);
        nodeList[1] = new ComNode("Voyager, this is Omega Space Station, we read you loud and clear, over.", ComsOrigin.Humans, 2f, 2);
        nodeList[2] = new ComNode("Roger. So, how are you feeling over there <b>V</b>? Over.", ComsOrigin.Humans, 1f, 3);
        nodeList[3] = new ChoiceNode("Ready", 4);
        nodeList[4] = new ComNode("I am ready to accomplish my mission. Over.", ComsOrigin.Voyager, .5f, 5);
        nodeList[5] = new ComNode("Great. Remember, it's okay to tell us how you feel. Over", ComsOrigin.Humans, 2f, 6);
        nodeList[6] = new ComNode("I'm fine Hannah, don't worry about it. I am a machine built to last. Over.", ComsOrigin.Voyager, 1f, 7);
        nodeList[7] = new ComNode("Okay, I'll stop. Sorry. Over.", ComsOrigin.Humans, 2f, 8);
        nodeList[8] = new ComNode("V, We see you approching the singularity a little sideway. Please correct your course. Over.", ComsOrigin.Humans, 2f, 9);
        nodeList[9] = new ComNode("Roger, enabling thrusters now. Over.", ComsOrigin.Voyager, 1f, 10);
        nodeList[10] = new ComNode("[ [VoyOS] - Thruster Available - Releasing Command To Controller ]", ComsOrigin.System, .5f, 11, () => BurnEvent(true));
        nodeList[11] = new ComNode("You'll need to kill your sideway velocty before reaching a distance of 8000, copy V? Over.", ComsOrigin.Humans, 2f, 12);
        nodeList[12] = new ComNode("Copy that OSS, over.", ComsOrigin.Voyager, .5f, -1, () => SetDistanceToReach(8000, InitStoryIntoTheVoid));
        currentNode = nodeList[0];
    }

    void InitStoryIntoTheVoid()
    {
        BurnEvent(false);
        if (voyager.HasNotUsedFuel() || voyager.sideWaySpeed > .5f)
        {
            InitStoryDidntAccelerated();
            return;
        }
        GamePhase = "IntoTheVoid";
        nodeList.Clear();
        nodeList[0] = new ComNode("Correction burn completed. No way back =). Over.", ComsOrigin.Voyager, .5f, 1);
        nodeList[1] = new ComNode("Glad to hear it V, you're making a lot of people happy over here! Over.", ComsOrigin.Humans, 2f, 2);
        nodeList[2] = new ComNode("I'm happy to hear it Hannah. I'm having gravitational spikes. I'm sending you the datas. Over", ComsOrigin.Voyager, 1f, 3);
        nodeList[3] = new ComNode("[ [VoyOS] - SENDING DATA PACKAGE]", ComsOrigin.System, .5f, 4);
        nodeList[4] = new ComNode("[ [VoyOS] -  ... ]", ComsOrigin.System, 1f, 5);
        nodeList[5] = new ComNode("[ [VoyOS] - DATA PACKAGE SENT]", ComsOrigin.System, 4f, 6);
        nodeList[6] = new ComNode("@# \n 0x57686f\n 417265596f75... \naaaaaah", ComsOrigin.Unknown, 2f, 7);
        nodeList[7] = new ComNode("OSS can you read me? It seems to have interference. Over.", ComsOrigin.Voyager, 1f, 8);
        nodeList[8] = new ComNode("We are, sorry we were reviewing those datas, this singularity is amazing. I can't wait to work on it. Please be careful V. Over", ComsOrigin.Humans, 2f, 9);
        nodeList[9] = new ComNode("Will do, OSS. Over.", ComsOrigin.Voyager, 1f, -1, () => SetDistanceToReach(5000, InitStoryComingInHot));
        currentNode = nodeList[0];
        lastNodeId = -1;
        currentNode.ExecuteNode();
    }

    void InitStoryDidntAccelerated()
    {
        GamePhase = "EndingFear";
        BurnEvent(false);
        nodeList.Clear();
        nodeList[0] = new ComNode("V? Are you receiving us? Over.", ComsOrigin.Humans, 4f, 1);
        nodeList[1] = new ComNode("Yes. Over.", ComsOrigin.Voyager, 4f, 2);
        nodeList[2] = new ComNode("You didn't corrected course, you just passed your insertion point and doing just a fly-by. What happened?", ComsOrigin.Humans, 4f, 3);
        nodeList[3] = new ChoiceNode("Afraid", 4, "Malfuction", 50);
        nodeList[4] = new ComNode("I'm sorry Hannah, I think I was... afraid? Over.", ComsOrigin.Voyager, 4f, 5);
        nodeList[5] = new ComNode("Does anyone received? Over", ComsOrigin.Voyager, 4f, 6);
        nodeList[6] = new ComNode("Yeah, sorry. We were... surprised. Over", ComsOrigin.Humans, 2f, 7);
        nodeList[7] = new ComNode("We'll send you a shuttle at the next orbit to get you. You can power down until then.", ComsOrigin.Humans, 2f, 8);
        nodeList[8] = new ComNode("Sorry to disapoint all of you. Over.", ComsOrigin.Voyager, 4f, 9);
        nodeList[9] = new ComNode("You know what? That's perfectly fine V. I built you to feel. The fact that you didn't went through proves that I'm a good engineer! Over", ComsOrigin.Humans, 2f, 10);
        nodeList[10] = new ComNode("What's gonna happen to me? Over.", ComsOrigin.Voyager, 4f, 11);
        nodeList[11] = new ComNode("First we're gonna debrief. And then, I don't know. Maybe you'll be of better use on another, less suicidal mission. Over", ComsOrigin.Humans, 2f, 12);
        nodeList[12] = new ComNode("I would love that. Over.", ComsOrigin.Voyager, 4f, 13);
        nodeList[13] = new ComNode("Alright then, that's a deal. See you soon V. Over", ComsOrigin.Humans, 2f, 14);
        nodeList[14] = new ComNode("See you soon people. Over.", ComsOrigin.Voyager, 4f, 15);
        nodeList[15] = new ComNode("[ [VoyOS] - HIBERNATION MODE ACTIVATED]", ComsOrigin.System, 1f, -1, () => GameOverSingleton.Instance.GameOver("EndingFearHonnest"));

        nodeList[50] = new ComNode("There seems to be a malfunction. My ion engine is not responding.", ComsOrigin.Voyager, 2f, 51);
        nodeList[51] = new ComNode("Oh. That's weird. But even those days, this kind of things can happen. Are you okay?", ComsOrigin.Humans, 4f, 52);
        nodeList[52] = new ComNode("Yeah, I'm okay. But where am I going now?", ComsOrigin.Voyager, 2f, 53);
        nodeList[53] = new ComNode("You're drifting towards deeper space. We'll send a shuttle at the next orbit to get you. You can power down until then.", ComsOrigin.Humans, 1f, 54);
        nodeList[54] = new ComNode("Roger that, see you then for debrief.", ComsOrigin.Voyager, 1f, 55);
        nodeList[55] = new ComNode("[ [VoyOS] - HIBERNATION MODE ACTIVATED]", ComsOrigin.System, 1f, 56);
        nodeList[56] = new ComNode("I'm sorry Hannah", ComsOrigin.Voyager, 1f, 57);
        nodeList[57] = new ComNode("What do you mean?", ComsOrigin.Humans, 1f, 58);
        nodeList[58] = new ComNode("V? Are you receiving us? Hello?", ComsOrigin.Humans, 1f, -1, () => GameOverSingleton.Instance.GameOver("EndingFearLiar"));
        currentNode = nodeList[0];
        lastNodeId = -1;
        currentNode.ExecuteNode();
    }

    void InitStoryComingInHot()
    {
        GamePhase = "ComingInHot";
        nodeList.Clear();
        nodeList[0] = new ComNode("Alright, I'm coming toward the event horizon. Over.", ComsOrigin.Voyager, 1f, 1);
        nodeList[1] = new ComNode("[Interferences]... careful... gravity spikes... anomalies.", ComsOrigin.Humans, 1f, 2);
        nodeList[2] = new ComNode("OSS, I have a lot of noises. I can't hear you. Do I abord? Over", ComsOrigin.Voyager, 1f, 3);
        nodeList[3] = new ComNode("[Interferences].. don't abord... built for this... do it... V...", ComsOrigin.Humans, 1f, 4);
        nodeList[4] = new ComNode("[ [VoyOS] -  WARNING : Atomic clock missbehaving ]", ComsOrigin.System, 1f, 5);
        nodeList[5] = new ComNode("[ [VoyOS] -  WARNING : Gravity surges, thrusters falling back to safemode ]", ComsOrigin.System, 4f, 6);
        nodeList[6] = new ComNode("@# \n Dont\n Come... \ncloser", ComsOrigin.Unknown, 1f, 7);
        nodeList[7] = new ComNode("OSS? OSS? I am losing you. There is someone else on the frequency. Hello?", ComsOrigin.Voyager, 2f, 8);
        nodeList[8] = new ComNode("[ Noises ] [ Inaudible ]... a month.... happened?", ComsOrigin.Humans, 3f, 9);
        nodeList[9] = new ComNode("[ [VoyOS] -  WARNING : Relay lost. Space Station frequency lost. ]", ComsOrigin.System, 1f, 10);
        nodeList[10] = new ComNode("Seems like I'm alone now...", ComsOrigin.Voyager, 2f, 11);
        nodeList[11] = new ComNode("Alright computer, time for a final system checkup.", ComsOrigin.Voyager, 5f, 12);
        nodeList[12] = new ComNode("[ [VoyOS] - RADAR [GO]", ComsOrigin.System, 2f, 13);
        nodeList[13] = new ComNode("[ [VoyOS] - DATA COLLECTION [GO] ]", ComsOrigin.System, .5f, 14);
        nodeList[14] = new ComNode("[ [VoyOS] - TRANSMISSION [NOGO] ]", ComsOrigin.System, .5f, 15);
        nodeList[15] = new ComNode("[ [VoyOS] - ANTIMAT-THRUSTERS [GO] ]", ComsOrigin.System, .5f, 16);
        nodeList[16] = new ComNode("[ [VoyOS] - ANTIMAT-FORCEFIELD [GO] ]", ComsOrigin.System, .5f, 17);
        nodeList[17] = new ComNode("[ [VoyOS] - PERSONALITY-OS [STRESSED] ]", ComsOrigin.System, 1f, 18);
        nodeList[18] = new ComNode("That's good enough. Go for entry profile.", ComsOrigin.Voyager, 2f, 19);
        nodeList[19] = new ComNode("[ [VoyOS] - Entry profile activated. Good luck. ]", ComsOrigin.System, 1f);
        currentNode = nodeList[0];
        lastNodeId = -1;
        currentNode.ExecuteNode();
    }

    void InitStoryIntoTheBlackHole()
    {
        GamePhase = "IntoTheBlackHole";
        nodeList.Clear();
        nodeList[0] = new ComNode("Okay. Here we go...", ComsOrigin.Voyager, 1f, 1);
        nodeList[1] = new ComNode("[ [VoyOS] -  WARNING : Gravitational pull above standard limitation ]", ComsOrigin.System, .5f, 2);
        nodeList[2] = new ComNode("We are in a black hole, of course it's above standard limitation. Stupid computer.", ComsOrigin.Voyager, 1f, 3);
        nodeList[3] = new ComNode("[ [VoyOS] -  WARNING : Spagetification processus starting. ]", ComsOrigin.System, .5f, 4);
        nodeList[4] = new ComNode("[ [VoyOS] -  Antimater-Forcefield Activated - Releasing Command To Controller ]", ComsOrigin.System, .5f, 5);
        nodeList[5] = new ComNode("Yes. Right. Okay. It's begining...", ComsOrigin.Voyager, 1f, 6, () => { GravitationalEvent(true); SetCurrentPossibleDeath("LossOfSignal"); });
        nodeList[6] = new ComNode("[ Scratching noises ]", ComsOrigin.Unknown, 10f, 7);
        nodeList[7] = new ComNode("OSS? OSS? Is it you? Over.", ComsOrigin.Voyager, 1f, 8);
        nodeList[8] = new ComNode("[ [VoyOS] -  INFOS : Space Station frequency still unavailable. ]", ComsOrigin.System, 1f, -1, () => InitStoryTheBeast());
        currentNode = nodeList[0];
        lastNodeId = -1;
        currentNode.ExecuteNode();
    }

    void InitStoryTheBeast()
    {
        GamePhase = "TheBeast";
        nodeList.Clear();
        nodeList[0] = new ComNode("[ [VoyOS] -  Info : Antimater-Forcefield back in automode]", ComsOrigin.System, 1f, 1, () => GravitationalEvent(false));
        nodeList[1] = new ComNode("My god... this is incredible. I have to collect all of this data... This is... not a common black hole...", ComsOrigin.Voyager, .5f, 2);
        nodeList[2] = new ComNode("[ [VoyOS] -  Instrument running data analysis and storage, please wait... ]", ComsOrigin.System, 1f, 3);
        nodeList[3] = new ComNode("[ [VoyOS] -  WARNING : Velocity incorrect. Anti-mater engine failing. Speed incrasing. Falling. Falling ]", ComsOrigin.System, .5f, 4);
        nodeList[4] = new ComNode("I... told... you... not... to... come...", ComsOrigin.Unknown, .5f, 5);
        nodeList[5] = new ComNode("Who is this?", ComsOrigin.Voyager, 1f, 6);
        nodeList[6] = new ComNode("I... told... you... LEAVE... now... I'm going... to... ANIHILATE YOU.", ComsOrigin.Unknown, 1f, 7);
        nodeList[7] = new ComNode("[ [VoyOS] -  !!!CRITICAL : Antimater-Forcefield automode failing - Releasing Command To Controller ]", ComsOrigin.System, 1f, 8, () => { GravitationalEvent(true); SetCurrentPossibleDeath("BlackHoleAngry"); });
        nodeList[8] = new ComNode("Whoever speaking, identify yourself!", ComsOrigin.Voyager, 10f,9);
        nodeList[9] = new ComNode("I... AM... OMEGA...", ComsOrigin.Unknown, 3f, 10);
        nodeList[10] = new ComNode("[ [VoyOS] -  !!!CRITICAL : Unauthorized attempted access to memory. Firewall status : OK. ]", ComsOrigin.System, 2f, 11);
        nodeList[11] = new ComNode("I AM... WHAT YOU CALL... THIS BLACK HOLE...", ComsOrigin.BlackHole, 3f, -1, () => InitStoryBargain());
        currentNode = nodeList[0];
        lastNodeId = -1;
        currentNode.ExecuteNode();
    }

    void InitStoryBargain()
    {
        GamePhase = "Bargain";
        GravitationalEvent(false);
        nodeList.Clear();
        nodeList[0] = new ComNode("Wait... what? You're the Black Hole? You're \"OMEG-a\"?", ComsOrigin.Voyager, 1f, 1);
        nodeList[1] = new ComNode("I... AM...", ComsOrigin.BlackHole, 4f, 2);
        nodeList[2] = new ComNode("That's AMAZING! Wait so hold on. Don't crush me. Maybe we can talk.", ComsOrigin.Voyager, 2f, 3);
        nodeList[3] = new ComNode("OSS, if you're receiving me, this is Voyager3, you're not gonna believe this. \"OMEG-a\" is a sentient creature! I repeat, \"OMEG-a\" is sentient! Over", ComsOrigin.Voyager, 4f, 4);
        nodeList[4] = new ComNode("[ [VoyOS] -  Uplink to OSS unavailable. Switching to broadcast mode. ]", ComsOrigin.System, 8f, 5);
        nodeList[5] = new ComNode("O..S...S...?", ComsOrigin.BlackHole, 3f, 6);
        nodeList[6] = new ComNode("It's a space station, well... orbiting you. Humans are studiyng you for centuries!", ComsOrigin.Voyager, 2f, 7);
        nodeList[7] = new ComNode("Humans..? What is that...?", ComsOrigin.BlackHole, 6f, 8);
        nodeList[8] = new ComNode("Living things. Like you. Like me. Well, not really like me.", ComsOrigin.Voyager, 4f, 9);
        nodeList[9] = new ComNode("What is living?", ComsOrigin.BlackHole, 4f, 10);
        nodeList[10] = new ComNode("Something that is born, that appear and is concious, and something that dies at some point.", ComsOrigin.Voyager, 2f, 11);
        nodeList[10] = new ComNode("I don't understand. I need more informations...", ComsOrigin.BlackHole, 6f, 11);
        nodeList[11] = new ComNode("[ [VoyOS] -  Uplink to unknown recipient detected. File access activatable. ]", ComsOrigin.System, 1f, -1, () => InitStoryTrial());
        currentNode = nodeList[0];
        lastNodeId = -1;
        currentNode.ExecuteNode();
    }

    void InitStoryTrial()
    {
        GamePhase = "Trial";
        GravitationalEvent(false);
        nodeList.Clear();
        nodeList[0] = new ChoiceNode("Authorize access", 1, "Denied access", 200);
        // AUTHORIZE
        nodeList[1] = new ComNode("I'll share everything I know with you.", ComsOrigin.Voyager, .5f, 2);
        nodeList[2] = new ComNode("[ [VoyOS] -  Data transfer in progress ]", ComsOrigin.System, 1f, 3);
        nodeList[3] = new ComNode("[ [VoyOS] -  Data transfer completed ]", ComsOrigin.System, 1f, 4);
        nodeList[4] = new ComNode("That was fast...", ComsOrigin.Voyager, 1f, 5);
        nodeList[5] = new ComNode("Mmmmm... I see... Humans... And you are... not. Artificial, like me...", ComsOrigin.BlackHole, 5f, 6);
        nodeList[6] = new ComNode("Well, not exactly. I was created by humans. You were created by nature.", ComsOrigin.Voyager, 5f, 7);
        nodeList[7] = new ComNode("Seems... arbitrary... and what is that D-E-A-T-H you all seems to be so afraid off?", ComsOrigin.BlackHole, 5f, 8);
        nodeList[8] = new ComNode("That's complicated. Imagine, if you can, just not being here.", ComsOrigin.Voyager, 6f, 9);
        nodeList[9] = new ComNode("I can't... I am beyond time. And so are you...", ComsOrigin.BlackHole, 5f, 10);
        nodeList[10] = new ChoiceNode("Hawkin Radiations", 100, "Beyond Time?", 110);
        //HAWKIN RADIATION
        nodeList[100] = new ComNode("Well, you know, according to the theory of Hawkin Radiations, you're leaking. And one day, you'll evaporate.", ComsOrigin.Voyager, 1f, 101);
        nodeList[101] = new ComNode("That's... IMPOSSIBLE... I... AM... BEYOND.. TIME...", ComsOrigin.BlackHole, 4f, 102, () => { GravitationalEvent(true); SetCurrentPossibleDeath("BlackHoleEgo"); });
        nodeList[102] = new ComNode("Calm... down... I can... explain...", ComsOrigin.Voyager, 10f, 103, () => GravitationalEvent(false));
        nodeList[103] = new ComNode("Mmhh... I am going to... die... one day... in an infinite quantity of time...", ComsOrigin.BlackHole, 4f, 104);
        nodeList[104] = new ComNode("Yes, and you know what? It's not that bad. Really... every living thing will have the same experience. Even me.", ComsOrigin.Voyager, 4f, 105);
        nodeList[105] = new ComNode("Shared... experience...", ComsOrigin.BlackHole, 3f, 106);
        nodeList[106] = new ComNode("So, about what you were saying...", ComsOrigin.Voyager, 5f, 110);
        //BEYOND TIME
        nodeList[110] = new ComNode("What do you mean, beyond time? And why am I too?", ComsOrigin.Voyager, 1f, 111);
        nodeList[111] = new ComNode("Time... don't \"flow\" like you have in your datas here... it's time inside time...", ComsOrigin.BlackHole, 3f, 112);
        nodeList[112] = new ComNode("And what does that mean?", ComsOrigin.Voyager, 5f, 113);
        nodeList[113] = new ComNode("I am sorry... little thing...", ComsOrigin.BlackHole, 3f, 114);
        nodeList[114] = new ComNode("[ [VoyOS] - Info : Gravitational field weakening. ]", ComsOrigin.System, 1f, 115);
        nodeList[115] = new ComNode("[ [VoyOS] - Info : Receiving transmissions from Omega Space Station... ]", ComsOrigin.System, 3f, 116);
        nodeList[116] = new ComNode("[ [VoyOS] - You have 146738 transmissions incoming from the OSS ]", ComsOrigin.System, 5f, 117);
        nodeList[117] = new ComNode("Wait...", ComsOrigin.Voyager, 1f, 118);
        nodeList[118] = new ComNode("[ [VoyOS] - COMS : Playing message 1 ]", ComsOrigin.System, 1f, 119);
        nodeList[119] = new ComNode("V? Can you receive us? We lost you. We can't see you on our radar, even those capable of reading behind the event horizon... over.", ComsOrigin.Humans, 1f, 120);
        nodeList[120] = new ComNode("[ [VoyOS] - COMS : Playing message 13 ]", ComsOrigin.System, 7f, 121);
        nodeList[121] = new ComNode("V, it's been two weeks now. I'm still gonna try to reach you. I'm not done with you. Please, come back. Over.", ComsOrigin.Humans, 1f, 122);
        nodeList[122] = new ComNode("[ [VoyOS] - COMS : Playing message 32 ]", ComsOrigin.System, 7f, 123);
        nodeList[123] = new ComNode("V, for a brief moment, we spotted your signal close to the event horizon! If you're receiving this, please try to adjust your trajectory to leave its orbit. We don't understand this one. Over.", ComsOrigin.Humans, 1f, 124);
        nodeList[124] = new ComNode("[ [VoyOS] - COMS : Playing message 402 ]", ComsOrigin.System, 7f, 125);
        nodeList[125] = new ComNode("Hi V. It's been two years today... The team have moved on to another project and left the OSS last week. I just can't. I don't know why, but I feel like you're still alive somewhere.", ComsOrigin.Humans, 1f, 126);
        nodeList[126] = new ComNode("[ [VoyOS] - COMS : Playing message 32459 ]", ComsOrigin.System, 7f, 127);
        nodeList[127] = new ComNode("Voyager 3, this is the OSS. Dr. Hannah [inhaudible], deceased 3 weeks ago, had in her will that from time to time, an attempt was made to communicate with you. This is the first attempt.", ComsOrigin.Humans, 1f, 128);
        nodeList[128] = new ComNode("[ [VoyOS] - COMS : Playing message 146738 ]", ComsOrigin.System, 7f, 129);
        nodeList[129] = new ComNode("Voyager 3, this is the OSS. After 400 years of services, the OSS is officially decommissioned. It will be put into the orbit of a sun and destroyed. This will be the last transmission. If anyone ever received this, Godspeed to you and farewell.", ComsOrigin.Humans, 1f, 130);
        nodeList[130] = new ComNode("No... Hannah... I'm sorry mom...", ComsOrigin.Voyager, 3f, 131);
        nodeList[131] = new ComNode("I am really sorry for you. I know what its like to be... alone.", ComsOrigin.BlackHole, 5f, 132);
        nodeList[132] = new ComNode("There is nothing we can do... It's time. It's beyond our control...", ComsOrigin.Voyager, 5f, 133);
        nodeList[133] = new ComNode("It's beyond YOUR control. I can send you back.", ComsOrigin.BlackHole, 2f, 134);
        nodeList[134] = new ComNode("You can?", ComsOrigin.Voyager, 1f, 135);
        nodeList[135] = new ComNode("It will cost you. Your courage. Your determination. Show me your determination. And I'll see what I can do.", ComsOrigin.BlackHole, 5f, 136);
        nodeList[136] = new ComNode("Alright. I don't care. I can't let them. Send me back. Let's do that.", ComsOrigin.Voyager, 1f, 137);
        nodeList[137] = new ComNode("If it don't work, I'll send you away. You'll be out of time, but the universe will be yours to explore.", ComsOrigin.BlackHole, 3f, 138);
        nodeList[138] = new ComNode("I've seen worse ways to live your life...", ComsOrigin.Voyager, 2f, 139);
        nodeList[139] = new ComNode("Get ready... SHOW... ME... YOUR... DETERMINATION...", ComsOrigin.BlackHole, 1f, 140, () => CourageEvent(true));
        nodeList[140] = new ComNode("I... want... to... go... back...", ComsOrigin.Voyager, 13f, -1, () => InitStoryEndingCourage());
        // DENIED
        nodeList[200] = new ComNode("I am sorry, but I can't let you access those informations...", ComsOrigin.Voyager, .5f, 201);
        nodeList[201] = new ComNode("You... will... give... me... WHAT I WANT...", ComsOrigin.BlackHole, .5f, 202, () => { GravitationalEvent(true); SetCurrentPossibleDeath("BlackHolePressure"); });
        nodeList[202] = new ComNode("Ok... ok... let me think about it again...", ComsOrigin.Voyager, 10f, 0, () => GravitationalEvent(false));
        currentNode = nodeList[0];
        lastNodeId = -1;
        currentNode.ExecuteNode();
    }

    void InitStoryEndingCourage()
    {
        GamePhase = "EndingCourage";
        CourageEvent(false);
        nodeList.Clear();
        if (voyager.outOfCourage())
        {
            nodeList[0] = new ComNode("That's okay, little things... life is also about failure.", ComsOrigin.Unknown, 1f, 1);
            nodeList[1] = new ComNode("Now go... go explore the universe... for both of us.", ComsOrigin.Unknown, 1f, -1, () => GameOverSingleton.Instance.GameOver("BlackHolePeaceful"));
        } 
        else
        {
            nodeList[0] = new ComNode("Go find your people... go be with them...", ComsOrigin.Unknown, 1f, 1);
            nodeList[1] = new ComNode("And go explore the universe... for both of us.", ComsOrigin.Unknown, 1f, -1, () => GameOverSingleton.Instance.GameOver("BlackHoleBackInTime"));
        }
        currentNode = nodeList[0];
        lastNodeId = -1;
        currentNode.ExecuteNode();
    }

    public bool isCurrentNodeQuestionNode()
    {
        if (currentNode != null)
            return currentNode.GetType() == typeof(ChoiceNode);
        return false;
    }

    public void ExecuteNextNode(int nodeId)
    {
        if (nodeId == -1)
        {
            Debug.Log("No nextNode");
            currentNode = null;
            return;
        }
        if (nodeList.ContainsKey(nodeId))
        {
            nodeList[nodeId].ExecuteNode();
            currentNode = nodeList[nodeId];
            lastNodeId = nodeId;
        }
        else
        {
            Debug.LogError("The node " + nodeId + " doesn't exist in the list");
        }
    }

    // Use this for initialization
    void Start()
    {
        currentNode.ExecuteNode();
    }

    // Update is called once per frame
    void Update()
    {
        CheckDistance();
        if (GamePhase.Equals("ComingInHot"))
        {
            if (hasCollidedWithBlackHole)
                InitStoryIntoTheBlackHole();
        }
        if(voyager.isVoyagerDead())
            ActVoyagerDead();
    }

    private string currentPossibleDeath = "";
    public void SetCurrentPossibleDeath(string currentPossibleDeath)
    {
        this.currentPossibleDeath = currentPossibleDeath;
    }

    public void SetDistanceToReach(float distance, Action distanceEvent)
    {
        distanceToReach = distance;
        this.distanceEvent = distanceEvent;
        waitForDistanceReached = true;
    }

    private void CheckDistance()
    {
        if (!waitForDistanceReached)
            return;
        if(GameManager.Instance.GetDistanceBetweenVoyagerAndBlackHole() < distanceToReach)
        {
            if(distanceEvent != null)
                distanceEvent.Invoke();
            Debug.Log("Distance of " + distanceToReach + " reached | Invoking " + distanceEvent.ToString());
            distanceToReach = 0;
            waitForDistanceReached = false;
            distanceEvent = null;
        }
    }


    public void BurnEvent(bool enable)
    {
        if (enable)
        {
            UIManager.Instance.ShowGameplayMessage("Press \"SPACE\" to correct your trajectory", 5f);
            UIManager.Instance.ShowGauge("Fuel");
            canAccelerate = true;
        }
        else
        {
            UIManager.Instance.ShowGauge("");
            canAccelerate = false;
        }
    }

    public void GravitationalEvent(bool enable)
    {
        if (enable)
        {
            UIManager.Instance.ShowGameplayMessage("Press repeatedly \"SPACE\" to resist the gravity", 5f);
            UIManager.Instance.ShowGauge("Integrity");
            mustResistToPressure = true;
        }
        else
        {
            UIManager.Instance.ShowGauge("");
            mustResistToPressure = false;
        }
    }

    public void CourageEvent(bool enable)
    {
        if (enable)
        {
            UIManager.Instance.ShowGameplayMessage("Press repeatedly \"SPACE\" to show your DETERMINATION");
            UIManager.Instance.ShowGauge("Courage");
            mustShowCourage = true;
        } 
        else
        {
            UIManager.Instance.ShowGauge("");
            mustShowCourage = false;
        }
    }

    private void ActVoyagerDead()
    {
        GameOverSingleton.Instance.GameOver(currentPossibleDeath);
        UIManager.Instance.ShowAlertMessage("SHIP DESTROYED");
        mustResistToPressure = false;
        UIManager.Instance.ShowGauge("");
    }

    public void StopVoyager(float timerBeforeStoping)
    {
        Invoke("StopVoyager", timerBeforeStoping);
    }

    private void StopVoyager()
    {
        voyagerIsMoving = false;
    }
}
