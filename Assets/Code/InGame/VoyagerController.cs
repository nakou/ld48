using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoyagerController : MonoBehaviour
{
    public float forwardSpeed = 1.5f;
    public float sideWaySpeed = 1.0f;

    public float healthPoints = 100;
    public float healthPointsMax = 100;
    public float fuel = 100f;
    public float fuelMax = 100f;
    public float courage = 100f;
    public float courageMax = 100f;

    public GameObject thrusterLight;
    public ParticleSystem thrusterParticles;

    public ParticleSystem damagesParticles;
    public ParticleSystem damagesParticles2;
    public ParticleSystem damagesParticles3;

    float pressureDamages = 1.2f;
    float courageDamages = 1.5f;

    float pressureHealing = 0;
    float courageHealing = 0;

    float secondFraction = 6f;

    float damageTimer = 0.2f;
    float damageTimerTemp = 0f;
    bool canDoDamages = false;

    // Start is called before the first frame update
    void Start()
    {
        damageTimer = 1 / secondFraction;
        if (ConfigManager.Instance != null) { 
            pressureDamages = pressureDamages * ConfigManager.Instance.difficultyDamagesMultiplicator;
            courageDamages = courageDamages * ConfigManager.Instance.difficultyDamagesMultiplicator;
            pressureHealing = pressureDamages * ConfigManager.Instance.difficultyHealingMultiplicator;
            courageHealing = courageDamages * ConfigManager.Instance.difficultyHealingMultiplicator;
        } 
        else
        {
            pressureHealing = pressureDamages * 1f;
            courageHealing = courageDamages * 1f;
        }
        fuel = fuelMax;
        healthPoints = healthPointsMax;
        courage = courageMax;
        ManageThrusterVisuals(false);
    }


    // Update is called once per frame
    void Update()
    {
        ManageDamagesEffects();
        if(damageTimerTemp > damageTimer)
        {
            damageTimerTemp = 0;
            canDoDamages = true;
        }
        damageTimerTemp += Time.deltaTime;
        if (Input.GetButton("Jump") && StoryManager.Instance.canAccelerate)
        {
            if(fuel > 0)
            {
                forwardSpeed *= 1.001f;
                if(sideWaySpeed > 0)
                {
                    sideWaySpeed -= .005f;
                }
                else
                {
                    sideWaySpeed = 0;
                }
                fuel -= .3f;
                UIManager.Instance.UpdateFuelGauge(FuelRatio());
                ManageThrusterVisuals(true);
                return;
            }
            else
            {
                ManageThrusterVisuals(false);
                UIManager.Instance.ShowAlertMessage("FUEL DEPLETED");
                return;
            }
        }
        if (StoryManager.Instance.mustResistToPressure)
        {
            if(canDoDamages)
                healthPoints -= pressureDamages;
            if (Input.GetButtonDown("Jump"))
            {
                healthPoints += pressureHealing;
            }
            UIManager.Instance.UpdateIntegrityGauge(HealthPointsRatio());
        }
        if (StoryManager.Instance.mustShowCourage)
        {
            if (canDoDamages)
                courage -= courageDamages;
            if (Input.GetButtonDown("Jump"))
            {
                courage += courageHealing;
            }
            UIManager.Instance.UpdateCourageGauge(CourageRatio());
        }
        ManageThrusterVisuals(false);
        canDoDamages = false;
    }

    public bool outOfCourage()
    {
        return courage <= 0;
    }

    public bool isVoyagerDead()
    {
        return healthPoints <= 0;
    }

    private void ManageDamagesEffects()
    {
        if (HealthPointsRatio() < .70f)
            if (Random.Range(0f, 1f) < .01f * (1/(1 - HealthPointsRatio())))
                damagesParticles.Play();
        if (HealthPointsRatio() < .50f)
            if (Random.Range(0f, 1f) < .01f * (1 /(1 - HealthPointsRatio())))
                damagesParticles2.Play();
        if (HealthPointsRatio() < .30f)
            if (Random.Range(0f, 1f) < .01f * (1 /(1 - HealthPointsRatio())))
                damagesParticles3.Play();
    }

    private void ManageThrusterVisuals(bool areActives)
    {
        thrusterLight.SetActive(areActives);
        if (areActives)
            thrusterParticles.Play();
        else
            thrusterParticles.Stop();
    }

    private void FixedUpdate()
    {
        if (!StoryManager.Instance.voyagerIsMoving)
            return;
        Vector3 direction = (GameManager.Instance.GetDirectionTowardBlackHole() * forwardSpeed) + (Vector3.left * sideWaySpeed);
        transform.Translate(direction);
    }

    public float HealthPointsRatio()
    {
        return healthPoints / healthPointsMax;
    }

    public float CourageRatio()
    {
        return courage / courageMax;
    }

    public float FuelRatio()
    {
        return fuel / fuelMax;
    }

    public bool HasNotUsedFuel()
    {
        return fuel >= fuelMax;
    }
}
