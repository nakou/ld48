﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{

    #region Singleton
    private static UIManager instance;
    public static UIManager Instance { get { return instance; } }

    private void Awake()
    {
        Debug.Log("UIManager is Awaking!");
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        InitManager();
    }
    #endregion

    [Header("COM Colors")]
    public Color systemColor;
    public Color voyagerColor;
    public Color humansColor;
    public Color blackholeColor;

    [Header("SySAlert Messages")]
    public GameObject alertMessageContener;
    public TextMeshProUGUI alertMessageContent;
    public GameObject gameplayInfosContener;
    public TextMeshProUGUI gameplayInfosContent;

    [Header("COM Messages")]
    public GameObject textBoxContener;
    public GameObject textBoxPrefab;
    public Button answerOne;
    public TextMeshProUGUI answerOneText;
    public Button answerTwo;
    public TextMeshProUGUI answerTwoText;
    public float textSpeed = .05f;
    bool isAddingContent = false;

    [Header("Radar infos")]
    public TextMeshProUGUI radarInfos;

    [Header("Gauges")]
    public TextMeshProUGUI gaugeTitle;
    public GameObject gaugeContener;
    public Image fuelGauge;
    public Image integrityGauge;
    public Image courageGauge;

    [Header("GameOver")]
    public Image blackScreen;
    public float blackScreenSpeed = 0.05f;

    void InitManager()
    {
        ShowGauge("");
        CloseGameplayMessage();
        CloseAlertMessage();
        answerOne.gameObject.SetActive(false);
        answerOneText.text = "";
        answerTwo.gameObject.SetActive(false);
        answerTwoText.text = "";
    }

    private void Start()
    {
        if (GameManager.Instance.debugSpeedText)
            textSpeed = .01f;
    }

    public void AddAnswerOne(string answer, int comNodeId)
    {
        AddAnswer(answerOne, answerOneText, answer, comNodeId);
    }

    public void AddAnswerTwo(string answer, int comNodeId)
    {
        AddAnswer(answerTwo, answerTwoText, answer, comNodeId);
    }

    private void AddAnswer(Button answerButton, TextMeshProUGUI answerButtonText, string answer, int comNodeId)
    {
        answerButton.gameObject.SetActive(true);
        answerButton.enabled = true;
        answerButtonText.text = answer;
        answerButton.onClick.AddListener(() => HideButtons());
        answerButton.onClick.AddListener(() => StoryManager.Instance.ExecuteNextNode(comNodeId));
        timerQuestionTemp = 0;
    }

    public void HideButtons()
    {
        answerOneText.text = "";
        answerTwoText.text = "";
        answerOne.onClick.RemoveAllListeners();
        answerTwo.onClick.RemoveAllListeners();
        answerOne.gameObject.SetActive(false);
        answerTwo.gameObject.SetActive(false);
    }

    public void AddComText(ComNode comNode)
    {
        string content = comNode.content;
        ComsOrigin comsOrigin = comNode.comsOrigin;
        float waitTime = comNode.waitTime;

        if (isAddingContent)
            return;

        isAddingContent = true;

        string color = "";
        string name = "";
        switch (comsOrigin)
        {
            case ComsOrigin.Voyager:
                color = ColorUtility.ToHtmlStringRGB(voyagerColor);
                name = "V";
                break;
            case ComsOrigin.Humans:
                color = ColorUtility.ToHtmlStringRGB(humansColor);
                name = "OSS";
                break;
            case ComsOrigin.System:
                color = ColorUtility.ToHtmlStringRGB(systemColor);
                name = "SYSTEM";
                break;
            case ComsOrigin.BlackHole:
                color = ColorUtility.ToHtmlStringRGB(blackholeColor);
                name = "OMEG-a";
                break;
            case ComsOrigin.Unknown:
                color = ColorUtility.ToHtmlStringRGB(blackholeColor);
                name = "??ORIGIN/ERROR??";
                break;
            default:
                break;
        }
        content = name + " : " + content;
        if (waitTime > 0)
            StartCoroutine(WaitingBeforeComs(waitTime, content, color, comNode));
        else
            StartCoroutine(WriteIntoTextbox(content, color, comNode));
    }

    private IEnumerator WaitingBeforeComs(float waitTime, string content, string color, ComNode comNode)
    {
        yield return new WaitForSeconds(waitTime);
        StartCoroutine(WriteIntoTextbox(content, color, comNode));
    }

    IEnumerator WriteIntoTextbox(string newDialogue, string color, ComNode comNode)
    {
        GameObject newTextBox = Instantiate(textBoxPrefab);
        newTextBox.transform.SetParent(textBoxContener.transform);
        newTextBox.transform.localScale = Vector3.one;
        TextMeshProUGUI textBox = newTextBox.GetComponent<TextMeshProUGUI>();
        string content = "";
        foreach (char c in newDialogue)
        {
            content += c;
            textBox.text = string.Format("<color=#{0}>{1}</color>", color, content);
            yield return new WaitForSeconds(textSpeed);
        }
        isAddingContent = false;
        comNode.GoNextNode();
        yield return null;
    }

    public void StartGameOverEffect()
    {
        blackScreen.gameObject.SetActive(true);
    }

    public void ShowGauge(string gaugeType)
    {
        gaugeTitle.text = gaugeType;
        gaugeContener.SetActive(true);
        fuelGauge.gameObject.SetActive(false);
        integrityGauge.gameObject.SetActive(false);
        courageGauge.gameObject.SetActive(false);
        switch (gaugeType)
        {
            case "":
                gaugeContener.SetActive(false);
                break;
            case "Courage":
                courageGauge.gameObject.SetActive(true);
                break;
            case "Integrity":
                integrityGauge.gameObject.SetActive(true);
                break;
            case "Fuel":
            default:
                fuelGauge.gameObject.SetActive(true);
                break;
        }
    }

    public void UpdateFuelGauge(float value)
    {
        fuelGauge.fillAmount = value;
    }

    public void UpdateIntegrityGauge(float value)
    {
        integrityGauge.fillAmount = value;
    }

    public void UpdateCourageGauge(float value)
    {
        courageGauge.fillAmount = value;
    }

    public void ShowAlertMessage(string message, float secondsStayOnScreen = 3)
    {
        if (alertMessageContener.activeSelf)
            return;
        alertMessageContent.text = "[ " + message + " ]";
        alertMessageContener.SetActive(true);
        Invoke("CloseAlertMessage", 3);
    }

    void CloseAlertMessage()
    {
        alertMessageContent.text = "";
        alertMessageContener.SetActive(false);
    }

    public void ShowGameplayMessage(string message, float secondsStayOnScreen = 3)
    {
        if (gameplayInfosContener.activeSelf)
            return;
        gameplayInfosContent.text = "[ " + message + " ]";
        gameplayInfosContener.SetActive(true);
        Invoke("CloseGameplayMessage", secondsStayOnScreen);
    }

    void CloseGameplayMessage()
    {
        gameplayInfosContent.text = "";
        gameplayInfosContener.SetActive(false);
    }

    void UpdateRadar(bool radarOk = true)
    {
        radarInfos.text = GameManager.Instance.GetDistanceToBlackHole();
        if (!radarOk)
        {
            radarInfos.text = "RADAR MALFUCTION : 0E_TOO_MUCH_DAMAGES";
            return;
        }
        if(StoryManager.Instance.GamePhase.Equals("Intro") || StoryManager.Instance.GamePhase.Equals("EndingFear"))
        {
            radarInfos.text = "DISTANCE TO OBJECTIVE = " + GameManager.Instance.GetDistanceToBlackHole() +
                "\nCURRENT SPEED = " + GameManager.Instance.GetCurrentSpeed() +
                "\nCORRECTION SPEED NEEDED = " + GameManager.Instance.GetCorrectionSpeedNeeded() + "";
        }
        if(StoryManager.Instance.hasCollidedWithBlackHole)
            radarInfos.text = "[RADAR MALFUCTION]\nDISTANCE TO OBJECTIVE = ERROR-NaN-INFINITE???";
    }

    public float autoResponseTimer = 5;
    float timerQuestionTemp = 0;
    bool autoResponseTriggered = false;

    private void ManageAutoResponse()
    {
        if (timerQuestionTemp > autoResponseTimer && !autoResponseTriggered)
        {
            if (answerTwo.gameObject.activeSelf)
                answerTwo.enabled = false;
            answerOne.onClick.Invoke();
            autoResponseTriggered = true;
        }
        timerQuestionTemp += Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (blackScreen.gameObject.activeSelf)
        {
            Color blackScreenColor = new Color(blackScreen.color.r, blackScreen.color.g, blackScreen.color.b, blackScreen.color.a + blackScreenSpeed);
            blackScreen.color = blackScreenColor;
            return;
        }
        UpdateRadar(GameManager.Instance.GetVoyagerController().HealthPointsRatio() > .3);
        if (StoryManager.Instance.isCurrentNodeQuestionNode())
        {
            ManageAutoResponse();
        }
    }
}
