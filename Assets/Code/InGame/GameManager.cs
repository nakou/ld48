﻿using UnityEngine;
using System.Collections.Generic;
using Cinemachine;

public class GameManager : MonoBehaviour
{

    #region Singleton
    private static GameManager instance;
    public static GameManager Instance { get { return instance; } }

    private void Awake()
    {
        Debug.Log("GameManager is Awaking!");
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        InitManager();
    }
    #endregion

    public AudioClip musicSpace;
    public AudioClip musicInside;
    public AudioClip ambianceSpace;
    public AudioClip ambianceInside;

    public AudioSource music;
    public AudioSource ambiance;

    public VoyagerController voyagerController;
    public GameObject blackHole;

    public Material insideBlackHoleSkybox;

    public CinemachineVirtualCamera voyagerCamera;

    public bool debugSpeedText = false;

    void InitManager()
    {
        
    }

    void Start()
    {
        
    }

    public void ChangeSkyBoxAndMusic()
    {
        RenderSettings.skybox = insideBlackHoleSkybox;
        music.clip = musicInside;
        music.Play();
        ambiance.clip = ambianceInside;
        ambiance.Play();
    }

    public VoyagerController GetVoyagerController()
    {
        return voyagerController;
    }

    public string GetDistanceToBlackHole()
    {
        return Vector3.Distance(voyagerController.gameObject.transform.position, blackHole.transform.position).ToString("0.00");
    }

    public string GetCurrentSpeed()
    {
        return (voyagerController.forwardSpeed + voyagerController.sideWaySpeed).ToString("0.00");
    }

    public string GetCorrectionSpeedNeeded()
    {
        return (voyagerController.sideWaySpeed).ToString("0.00");
    }

    public float GetDistanceBetweenVoyagerAndBlackHole()
    {
        return Vector3.Distance(voyagerController.gameObject.transform.position, blackHole.transform.position);
    }

    public Vector3 GetDirectionTowardBlackHole()
    {
        return (blackHole.transform.position - voyagerController.transform.position).normalized;
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel") && Application.isEditor)
        {
            GameOverSingleton.Instance.GameOver("DEBUG");
        }
    }
}
